package com.example.foyer_koubaa_youssef.Services;


import com.example.foyer_koubaa_youssef.DAO.Entities.Chambre;
import com.example.foyer_koubaa_youssef.DAO.REPO.ChambreRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service //Definir que c'est un bean Spring
@AllArgsConstructor //Pour injecter un bean spring dans un autre bean
public class ChambreService implements IChambreService{
       ChambreRepository chambreRepository;
       @Override
       public Chambre addChambre(Chambre chambre) {

              return chambreRepository.save(chambre);
       }

       @Override
       public List<Chambre> addAllChambres(List<Chambre> chambres) {

              return chambreRepository.saveAll(chambres);
       }

       @Override
       public Chambre updateChambre(Chambre chambre) {

              return chambreRepository.save(chambre);
       }

       @Override
       public List<Chambre> updateAllChambres(List<Chambre> chambres) {

              return chambreRepository.saveAll(chambres);
       }

       @Override
       public List<Chambre> findAllChambres() {

              return chambreRepository.findAll();
       }

       @Override
       public Chambre findChambreById(long id) {

              return chambreRepository.findById(id).orElse(null);
       }

       @Override
       public void deleteChambre(Chambre chambre) {

              chambreRepository.delete(chambre);
       }

       @Override
       public void deleteChambreById(long id) {

              chambreRepository.deleteById(id);
       }
}
