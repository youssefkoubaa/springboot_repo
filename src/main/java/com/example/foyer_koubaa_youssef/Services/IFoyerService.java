package com.example.foyer_koubaa_youssef.Services;

import com.example.foyer_koubaa_youssef.DAO.Entities.Foyer;

import java.util.List;

public interface IFoyerService {

    Foyer addFoyer(Foyer foyer);

    List<Foyer> addAllFoyers(List<Foyer> foyers);

    Foyer updateFoyer(Foyer foyer);

    List<Foyer> updateAllFoyers(List<Foyer> foyers);

    List<Foyer> findAllFoyers();

    Foyer findFoyerById(long id);

    void deleteFoyer(Foyer foyer);

    void deleteFoyerById(long id);
}
