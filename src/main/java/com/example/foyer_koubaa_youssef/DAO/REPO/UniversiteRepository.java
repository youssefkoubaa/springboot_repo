package com.example.foyer_koubaa_youssef.DAO.REPO;


import com.example.foyer_koubaa_youssef.DAO.Entities.Universite;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UniversiteRepository extends JpaRepository<Universite,Long> {

//    - Afficher la liste des universités qui ont des étudiants dont leurs noms contiennet la chaine de caractère en paramètre et leurs dates de naissance entre deux dates passées en paramètre
List<Universite> findUniversitiesByNameAndBirthdateRange();
}
