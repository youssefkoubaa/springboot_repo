package com.example.foyer_koubaa_youssef.DAO.REPO;


import com.example.foyer_koubaa_youssef.DAO.Entities.Foyer;
import com.example.foyer_koubaa_youssef.DAO.Entities.TypeChambre;
import org.springframework.data.jpa.repository.JpaRepository;

import java.lang.reflect.Type;
import java.util.List;

public interface FoyerRepository extends JpaRepository<Foyer, Long> {

    Foyer findByNomFoyer(String nom);

    List<Foyer> findByCapaciteFoyerIsGreaterThan(long capaciteFoyer);

    List<Foyer> findByCapaciteFoyerIsLessThan(long capaciteFoyer);

    List<Foyer> findByCapaciteFoyerIsBetween(long mincapaciteFoyer, long maxcapaciteFoyer);
    //le foyer de l'universite dont son nom est parametre
    // select f from foyer f join universite u on <condition jointure> where

    Foyer findByUniversiteNomUnivrersite(String nom);

    //afficher la liste des foyers qui ont des chambres de meme type que le type passé en paramétre
    //Foyer--Bloc--Chambre
    List<Foyer> getByBlocsChambres(TypeChambre typeChambre);

    List<Foyer> findByBlocsNomBloc(String nomBloc);

    // 2- Recherche du foyer par son idFoyer pour un bloc donné
    Foyer findByIdAndBlocsNomBloc(Long id, String nomBloc);

    // 3- Recherche des foyers d'un bloc ayant une capacité spécifique
    List<Foyer> findByBlocsNomBlocAndCapaciteFoyer(String nomBloc, long capaciteFoyer);

    // 4- Recherche du foyer d'un bloc spécifique dans une université donnée
    Foyer findByBlocsNomBlocAndUniversiteNomUniversite(String nomBloc, String nomUniversite);
}
