package com.example.foyer_koubaa_youssef.DAO.REPO;


import com.example.foyer_koubaa_youssef.DAO.Entities.Etudiant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface EtudiantRepository extends JpaRepository<Etudiant,Long> {

    //select *from Etudiant where cin=...
    Etudiant findByCin(long cin);
    //select * from etudiant where nomEt like....
    List<Etudiant> findByNomELike(String nom);
    List<Etudiant> findByNomEContains(String nom);
    List<Etudiant> findByNomEContaining(String nom);


    Etudiant findByNomAndPrenom(String nom, String prenom);

    Etudiant findByCinAndNomE(long cin, String nomE);

    List<Etudiant> findByNaissanceAfter(LocalDate date);

    List<Etudiant> findByEcole(String ecole);

    List<Etudiant> findByReservations_Id(Long reservationId);

    List<Etudiant> findByReservations_Annee(int annee);

    List<Etudiant> findByEcoleAndNaissanceAfter(String ecole, LocalDate date);

    List<Etudiant> findByReservations_AnneeOrderByNaissance(int annee);

}

