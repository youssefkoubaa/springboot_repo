package com.example.foyer_koubaa_youssef.DAO.Entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "Universite")
public class Universite implements Serializable {

    @OneToOne
    private Foyer f;

    @Id
    @Column(name="idUniversite")
    @GeneratedValue(strategy = GenerationType.IDENTITY) //autoincrement = identity
    private long id;
    private String nomUnivrersite;
    private  long adresse;
}
