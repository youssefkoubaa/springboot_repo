package com.example.foyer_koubaa_youssef.DAO.Entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "Foyer")

public class Foyer implements Serializable {

    @OneToOne
    private Universite universite;

    @Id
    @Column(name="idFoyer")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nomFoyer;
    private long capaciteFoyer;


    @OneToMany(cascade = CascadeType.ALL, mappedBy="f")
    private Set<Bloc> blocs;
}