package com.example.foyer_koubaa_youssef.RestControllers;

import com.example.foyer_koubaa_youssef.DAO.Entities.Reservation;
import com.example.foyer_koubaa_youssef.Services.IReservationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor

public class ReservationRestController {

    private final IReservationService reservationService;

    @GetMapping("/getAllReservations")
    public List<Reservation> getAllReservations() {
        return reservationService.findAllReservations();
    }

    @PostMapping("/addReservation")
    public Reservation addReservation(@RequestBody Reservation reservation) {
        return reservationService.addReservation(reservation);
    }

    @PostMapping("/addAllReservations")
    public List<Reservation> addAllReservations(@RequestBody List<Reservation> reservations) {
        return reservationService.addAllReservations(reservations);
    }

    @GetMapping("/getReservationById")
    public Reservation getReservationById(@RequestParam long id) {
        return reservationService.findReservationById(id);
    }

    @GetMapping("/getReservationById/{id}")
    public Reservation getReservationByid(@PathVariable long id) {
        return reservationService.findReservationById(id);
    }

    @PutMapping("/updateReservation")
    public Reservation updateReservation(@RequestBody Reservation updatedReservation) {
        return reservationService.updateReservation(updatedReservation);
    }

    @PutMapping("/updateAllReservations")
    public List<Reservation> updateAllReservations(@RequestBody List<Reservation> updatedReservations) {
        return reservationService.updateAllReservations(updatedReservations);
    }

    @DeleteMapping("/deleteReservation")
    public void deleteReservation(@RequestBody Reservation reservation) {
        reservationService.deleteReservation(reservation);
    }

    @DeleteMapping("/deleteReservationById/{id}")
    public void deleteReservationById(@PathVariable long id) {
        reservationService.deleteReservationById(id);
    }
}
