package com.example.foyer_koubaa_youssef.RestControllers;

import com.example.foyer_koubaa_youssef.DAO.Entities.Etudiant;
import com.example.foyer_koubaa_youssef.Services.IEtudiantService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class EtudiantRestController {

     IEtudiantService etudiantService;

    @GetMapping("/getAllEtudiants")
    public List<Etudiant> getAllEtudiants() {
        return etudiantService.findAllEtudiants();
    }

    @PostMapping("/addEtudiant")
    public Etudiant addEtudiant(@RequestBody Etudiant etudiant) {
        return etudiantService.addEtudiant(etudiant);
    }

    @PostMapping("/addAllEtudiants")
    public List<Etudiant> addAllEtudiants(@RequestBody List<Etudiant> etudiants) {
        return etudiantService.addAllEtudiants(etudiants);
    }

    @GetMapping("/getEtudiantById")
    public Etudiant getEtudiantById(@RequestParam long id) {
        return etudiantService.findEtudiantById(id);
    }

    @GetMapping("/getEtudiantById/{id}")
    public Etudiant getEtudiantByid(@PathVariable long id) {
        return etudiantService.findEtudiantById(id);
    }

    @PutMapping("/updateEtudiant")
    public Etudiant updateEtudiant(@RequestBody Etudiant updatedEtudiant) {
        return etudiantService.updateEtudiant(updatedEtudiant);
    }

    @PutMapping("/updateAllEtudiants")
    public List<Etudiant> updateAllEtudiants(@RequestBody List<Etudiant> updatedEtudiants) {
        return etudiantService.updateAllEtudiants(updatedEtudiants);
    }

    @DeleteMapping("/deleteEtudiant")
    public void deleteEtudiant(@RequestBody Etudiant etudiant) {
        etudiantService.deleteEtudiant(etudiant);
    }

    @DeleteMapping("/deleteEtudiantById/{id}")
    public void deleteEtudiantById(@PathVariable long id) {
        etudiantService.deleteEtudiantById(id);
    }
}
