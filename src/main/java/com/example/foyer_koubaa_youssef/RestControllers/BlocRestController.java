package com.example.foyer_koubaa_youssef.RestControllers;

import com.example.foyer_koubaa_youssef.DAO.Entities.Bloc;
import com.example.foyer_koubaa_youssef.Services.IBlocService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class BlocRestController {
    IBlocService iBlocService;
    // read
    @GetMapping("getAllBlocs")
    public List<Bloc> getAll() {
        return iBlocService.findAllBlocs();
    }
    //create
    @PostMapping("addbloc")
    public Bloc addbloc(@RequestBody Bloc b) {
        return iBlocService.addBloc(b);
    }

    @PostMapping("addallblocs")
    public List<Bloc> addAllBlocs(@RequestBody List<Bloc> blocs){
        return iBlocService.addAllBlocs(blocs);

    }

    @GetMapping("getById")
    public Bloc getById(@RequestParam long id) {
        return iBlocService.findBlocById(id);
    }

    @GetMapping("getById/{id}")
    public Bloc getById2(@PathVariable long id) {
        return iBlocService.findBlocById(id);


    }
    @PutMapping("updateBloc")
    public Bloc updateBloc(@RequestBody Bloc b) {
        return iBlocService.updateBloc(b);
    }
    @PutMapping("updateAllBlocs")
    public List<Bloc> updateAllBlocs(@RequestBody List<Bloc> blocs){
        return iBlocService.updateAllBlocs(blocs);
    }

    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@PathVariable long id) {
        iBlocService.deleteBlocById(id);
    }



}
