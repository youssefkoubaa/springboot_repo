package com.example.foyer_koubaa_youssef.RestControllers;

import com.example.foyer_koubaa_youssef.DAO.Entities.Foyer;
import com.example.foyer_koubaa_youssef.Services.IFoyerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class FoyerRestController {

    IFoyerService foyerService;

    @GetMapping("/getAllFoyers")
    public List<Foyer> getAllFoyers() {
        return foyerService.findAllFoyers();
    }

    @PostMapping("/addFoyer")
    public Foyer addFoyer(@RequestBody Foyer foyer) {
        return foyerService.addFoyer(foyer);
    }

    @PostMapping("/addAllFoyers")
    public List<Foyer> addAllFoyers(@RequestBody List<Foyer> foyers) {
        return foyerService.addAllFoyers(foyers);
    }

    @GetMapping("/getFoyerById")
    public Foyer getFoyerById(@RequestParam long id) {
        return foyerService.findFoyerById(id);
    }

    @GetMapping("/getFoyerById/{id}")
    public Foyer getFoyerByid(@PathVariable long id) {
        return foyerService.findFoyerById(id);
    }

    @PutMapping("/updateFoyer")
    public Foyer updateFoyer(@RequestBody Foyer updatedFoyer) {
        return foyerService.updateFoyer(updatedFoyer);
    }

    @PutMapping("updateAllFoyers")
    public List<Foyer> updateAllFoyers(@RequestBody List<Foyer> updatedFoyers) {
        return foyerService.updateAllFoyers(updatedFoyers);
    }

    @DeleteMapping("deleteFoyer")
    public void deleteFoyer(@RequestBody Foyer foyer) {
        foyerService.deleteFoyer(foyer);
    }

    @DeleteMapping("/deleteFoyerById/{id}")
    public void deleteFoyerById(@PathVariable long id) {
        foyerService.deleteFoyerById(id);
    }


}
