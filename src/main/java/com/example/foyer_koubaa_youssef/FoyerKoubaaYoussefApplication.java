package com.example.foyer_koubaa_youssef;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoyerKoubaaYoussefApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoyerKoubaaYoussefApplication.class, args);
    }

}
